#!/usr/bin/python
# coding:utf-8

''' @file  fitsql.py
This script has ability to index FIT files into database 

  Creates SQL script for create table from example FITS file.
  @command ./fitsql.py create config_category
  @arg sql_file - sql file where the output will be stored
  @arg config_category - category in CONFFILE (needed tablename, reference_fit(abspath))

  Creates table,installs table and index first data from datasource path in config file
  @command ./fitsql.py import config_category
  @arg config_category - category in CONFFILE (needed tablename, datasource, reference_fit(abspath))

'''

import sys
import os
import io
import pyfits
import datetime
import logging
from exceptions import Exception
import cPickle

from fitslib import utils
from fitslib.utils import CONFFILE, get_conf

from fitslib.ssap_mapping import SSAP102_Fields as fields
from fitslib.ssap_mapping import FITS_VOTable as mapping
from fitslib.ssap_mapping import pgsphere_map
from fitslib.ssap_mapping import mandatory_map

APPDIR = os.path.abspath(os.path.dirname(__file__))

LOGFILE = "fitsql.log"
METADIR = "meta/"
SQLDIR = "sql/"

DEBUG = False

if not os.path.exists(METADIR):
  os.mkdir(METADIR, 0774)

if not os.path.exists(SQLDIR):
  os.mkdir(SQLDIR, 0774)
  
try:
  DEBUG = get_conf(sys.argv[2].strip()).get('debug', False)
except:
  pass
  
logging.basicConfig(
    level = logging.DEBUG,
    format = '%(asctime)s %(levelname)s %(message)s'
  )
  
def help():
  print """\
  
Commands:
  create section - creates sql/{section}.create.sql file with tables creation statements
  import section - creates sql/{section}.insert.sql file with insert statements
  
Help:
  COMMAND ?
  COMMAND h
  COMMAND help\
  
  """

def _get_local_map(meta):
  import imp
  
  try:
    f,filename,desc = imp.find_module(meta, ["mapping",])
    imp.load_module("mapping.%s" % meta, f, filename, desc)
    logging.info("Imported module %s from mapping" % meta)
    
    return sys.modules["mapping.%s" % meta].mapping
    
  except SyntaxError as se:
    logging.error("Syntax error in module  %s" % meta)
    logging.error(se)
  
  except Exception as e:
    logging.info("No valid mapping module found for %s" % meta)
    logging.info(e)

  return None
  
  
def _get_key(key):
  return "KEY_" + key.replace("-", "__").upper()
  

def _apply_mapping(cards, mapping):
  out = []
  for mm in mapping:
    try:
      args = [ cards[k].value for k in mm['keys'] ]
      out.append( utils.TableCol( **mm['map'](*args) ) )
    except:
      # we did not manage applicate current mapping
      pass
  return out
  
  
def h_install():
  print """\
  
Vytvori v PostgreSQL databazi %s a v ni tabulku admin.\
      
  """ % database

def p_install():
  sql = """

CREATE TABLE admin (
  name varchar(20) NOT NULL,
  path varchar(50) NOT NULL,
  PRIMARY KEY (name)
);

  """
  
  db = connect("test")
  db.autocommit = 1
  kurzor = db.cursor()
  sql_execute(kurzor, "CREATE DATABASE %s" % (database))
  kurzor.close()
  db.close()

  db = connect()
  kurzor = db.cursor()
  sql_execute(kurzor, sql)
  kurzor.close()
  db.commit()
  print "install\nFinish..."

def h_reset():
  print """\

Drops all created tables and view for [section]. Removes all meta files (mapping will be preserved)
      
  """ % database
  
def p_reset(meta=None):
  
  if meta is None:
    meta = sys.argv[2]
  conf = get_conf(meta)
    
  response = raw_input("Reset section? (y/[n]) ")
  
  data = '''
  DROP VIEW %(tablename)s_VO;
  DROP TABLE %(tablename)s;
  DROP TABLE %(tablename)s_META;
  '''
  
  if (response.lower() in ["y", "yes"]):
    _rm_file(os.path.join(METADIR, "%s_last_index" % meta))
    _rm_file(os.path.join(METADIR, "%s.map" % meta))

    _rm_file(os.path.join(SQLDIR, "%s.create.sql" % meta))
    _rm_file(os.path.join(SQLDIR, "%s.insert.sql" % meta))
  
    conn = utils.Connector(meta)
    conn.execute(data % {'tablename' : meta.upper()})
    conn.close()
    
    print "Done"
  else:
    print "Aborted"

def _rm_file(fl):
  if os.path.exists(fl):
    os.remove(fl)


def h_create():
  print """\

Usage: ./fitsql.py create section
Generates sql/{section}.create.sql provided existence of reference_fit file and tablename
given in config file for the section.
For more information see manual in doc/manual.pdf
      
  """
  
def p_create(meta=None):
  '''
  Creates SQL script for create tables
  '''
  try:
    if meta is None: meta = sys.argv[2].strip()
    conf = get_conf(meta, ('tablename', "reference_fit"))
    pgsphere = conf.get("pgsphere", False)
    tablename = conf["tablename"].upper()
    fitsfile = conf["reference_fit"]
    if not os.path.exists(fitsfile): raise KeyError("Example file does not exist")
    
  except Exception as e:
    logging.error(e)
    h_create()
    sys.exit()
  
  # now import additonal mapping
  local_map = _get_local_map(meta)
  
  # where the output will be stored
  sqlfile = os.path.join(SQLDIR, "%s.create.sql" % meta )
    
  #pgsphere = bool(conf.get("pgsphere", False))
  engine = conf.get("engine", 'mysql')
  
  fimg = pyfits.open(fitsfile)
  cards = fimg[0].header.ascard
  
  # list of TableCol which represents physical table
  table_cols = [
    utils.TableCol("META_CTIME", "BIGINT"),
    utils.TableCol("META_FILENAME", "varchar(50)"),
    utils.TableCol("META_PERM", "smallint", "NULL"),
  ]
  
  # list of DBValue instances which defines every COLUMN in VIEW
  meta_rows = []
  
  # list of tuples (FITS_COL, VOTable_COL)
  view_defs = []
  
  # list of tuples used for future mapping FIT file into database
  types_map = {'meta' : [], 'fit' : []}
  
  # apply all mappings
  table_cols.extend(_apply_mapping(cards, mandatory_map))
  if pgsphere:    
    table_cols.extend(_apply_mapping(cards, pgsphere_map))
  if local_map:
    table_cols.extend(_apply_mapping(cards, local_map))
  
  # save META part of map
  for col in table_cols:
    types_map['meta'].append((col.key, col.typename))
    
  for card in cards:
    key = _get_key(card.key)
    col = utils.TableCol(key)
    meta_dict = {}
    
    if (card.value.__class__ != bool):

      # crucial function of all the times
      col = _get_col_from_card(card, key, meta_dict)
      if col is None: continue
      types_map['fit'].append( (card.key, col.typename) )
      
      # FITS table column append
      col.comment = str(card.value)
      table_cols.append(col)
      
      vo_key = key
      if mapping.has_key(card.key):
        vo_key = mapping[card.key]
        # VIEW from FITS column to VO column
        view_defs.append( (key, vo_key,) )
        meta_dict.update(fields[vo_key])
      
      # META table add data
      meta_rows.extend(_dict_to_meta(key, meta_dict)) #@TODO Should there be a FITS keyword??
      
      # end if card.value.__class__ == something
      
    # end if card.value.__class__ != bool

  # end for
  fimg.close()
  
  # serialize types_map for future
  with open(os.path.join(METADIR, "%s.map" % meta), "w") as f:
    cPickle.dump(types_map, f)
  
  data_table_head = {
  'postgresql' : """
CREATE TABLE %(tablename)s (
  ID             SERIAL,
""",
  'mysql' : """
CREATE TABLE %(tablename)s (
  ID               BIGINT		AUTO_INCREMENT,
""",
}

  data_table_tail = {
  'postgresql' : """PRIMARY KEY(ID)
  );
  
""",
  'mysql' : """PRIMARY KEY(ID)
  );
  
""",
}
  
  meta_table_head = {
  'postgresql' :"""
CREATE TABLE %(tablename)s_META (
  ID    SERIAL,
  KEYWORD   varchar(20) NOT NULL,
  ATTRNAME  varchar(20) NOT NULL,
  ATTRVAL varchar(20) NOT NULL,
  PRIMARY KEY (ID)
);
  
INSERT INTO %(tablename)s_META (KEYWORD, ATTRNAME, ATTRVAL) VALUES
""",
  'mysql' :"""
CREATE TABLE %(tablename)s_META (
  ID    INT AUTO_INCREMENT,
  KEYWORD   varchar(20) NOT NULL,
  ATTRNAME  varchar(20) NOT NULL,
  ATTRVAL varchar(20) NOT NULL,
  PRIMARY KEY (ID)
);
  
INSERT INTO %(tablename)s_META (KEYWORD, ATTRNAME, ATTRVAL) VALUES
"""
}
  
  view_head = {
  'postgresql' :"""
# This is auto generated VIEW - edit as you wish
CREATE VIEW %(tablename)s_VO AS
SELECT
""",
  'mysql' :"""
# This is auto generated VIEW - edit as you wish
CREATE VIEW %(tablename)s_VO AS
SELECT
"""
}
  view_tail = {
  'postgresql' :"""\tID AS ID
FROM %(tablename)s;
""",
  'mysql' :"""\tID AS ID
FROM %(tablename)s;
"""
}

  with io.BufferedWriter(io.FileIO(sqlfile, "w"), 2048) as bfo:
	  
    # writing SQL create for DATA table
    bfo.write(data_table_head[engine] % {"tablename" : tablename})
    for col in table_cols:
      bfo.write("  " + col.sql_create())
    bfo.write(data_table_tail[engine] % {"tablename" : tablename})
  
    # writing SQL create for META table
    bfo.write(meta_table_head[engine] % {"tablename" : tablename})
    for DBValue in meta_rows[:-1]:
      bfo.write("  " + str(DBValue) + ",\n")
    bfo.write("  " + str(meta_rows[-1]) + ";\n")
    
    # writeing SQL create for VOTABLE view
    bfo.write(view_head[engine] % {"tablename" : tablename})
    for view_def in view_defs:
      bfo.write("\t%s AS %s,\n" % view_def)
    bfo.write(view_tail[engine] % {"tablename" : tablename})
    
  print "SQL file written to %s" % sqlfile


def _get_col_from_card(card, key, meta_dict):
  if card.key == "":    return None
  if "!" in key:        return None
  if "COMMENT" in key:  return None
  
  col = utils.TableCol(key)
  
  if (card.value.__class__ == int): 
    col.typename = "long"
    col.value = card.value
    meta_dict['datatype'] = 'long'
  elif (card.value.__class__ == float): 
    col.typename = "double"
    col.value = card.value
    meta_dict['datatype'] = 'double'
    meta_dict['precision'] = '16'
  elif isinstance(card.value, str):
    if len(card.value) == 0:
      col.typename = "varchar(72)"
      meta_dict['datatype'] ='char'
      meta_dict['arraysize'] ='*'
      col.value = card.value
      
    elif card.value[0].isdigit(): # probably a date/time, angle ...
      try:
        dt = utils.parse_datetime(card.value)
        if dt is not None:
          if dt.__class__ == datetime.time:
            col.typename = "time"
            meta_dict['datatype'] = 'char'
            meta_dict['arraysize'] = '8*'
            meta_dict['xtype'] = 'time'
            col.value = dt
            raise utils.SuccessError
          elif dt.__class__ == datetime.date:
            col.typename = "date"
            meta_dict['datatype'] = 'char'
            meta_dict['arraysize'] = '10*'
            meta_dict['xtype'] = 'date'
            col.value = dt
            raise utils.SuccessError
          elif dt.__class__ == datetime.datetime :
            col.typename = "datetime"
            meta_dict['datatype'] = 'char'
            meta_dict['arraysize'] = '19'
            meta_dict['xtype'] = 'datetime'
            col.value = dt
            raise utils.SuccessError
            
        fl = utils.parse_DMS(card.value)
        if fl is not None:
          if "RA" in card.key.upper():
            fl *= 15.0
          col.typename = "double"
          meta_dict['datatype'] = 'double'
          meta_dict['precision'] = '16'
          col.value = fl
          raise utils.SuccessError
        
        try:
          fl = float(card.value.strip())
          col.typename = "double"
          meta_dict['datatype'] = 'double'
          meta_dict['precision'] = '16'
          col.value = fl
          raise utils.SuccessError
        except ValueError:
          col.typename = "varchar(72)"
          meta_dict['datatype'] ='char'
          meta_dict['arraysize'] ='*'
          col.value = card.value
          raise utils.SuccessError
          
      except utils.SuccessError: # we've found right format. Now continue normally
        pass
        
    else: # take value as string
      col.typename = "varchar(72)"
      meta_dict['datatype'] ='char'
      meta_dict['arraysize'] ='*'
      col.value = card.value
  
  elif card.value.__class__ == pyfits.card.Undefined:
    # if the value is None just skip this row
    return None
        
  else: # default data type
    col.typename = "varchar(72)"
    meta_dict['datatype'] = 'char'
    meta_dict['arraysize'] = '*'
    col.value = card.value
  
  if card.comment is not '':
    meta_dict['description'] = card.comment
  
  return col

def _dict_to_meta(key, d):
  '''Takes dictionary of possible meta informations about column and returns list of DBValue'''
  out = []
  for k in d:
    out.append( utils.DBValue(key, k, d[k]) )
  return out
  
  
def h_import():
  print """\
Usage: ./fitsql.py import section
Creates {section}.insert.sql file where are INSERT statements to
insert unindexed FIT files into database.

For more information see manual in doc/manual.pdf
  """

def p_import(fitsfile=None):
  '''
  Creates SQL script which insert data into table
  '''
  try:
    meta = sys.argv[2].strip()
    conf = get_conf(meta, ('tablename', 'datasource', "reference_fit"))
    tablename = conf["tablename"].upper()
    data_source = conf["datasource"]
    if not os.path.exists(data_source): raise KeyError("Datasource path does not exist")

    fitsfile = conf["reference_fit"]
    if not os.path.exists(fitsfile): raise KeyError("Example file does not exist")

  except Exception as e:
    logging.error(e)
    h_import()
    sys.exit()
    
  pgsphere = conf.get("pgsphere", False)  # value from config
      
  # try to import user defined mapping
  local_map = _get_local_map(meta)
        
  pgsphere = bool(conf.get("pgsphere", False))
  engine = conf.get("engine", 'mysql')

  logging.debug('engine: ' + engine)
  logging.debug('pgsphere: ' + str(pgsphere))
  
  ## I know it is ugly to have it hardcoded in string. One day I'll rewrite it
  data_table_head = {
  'postgresql' : """
INSERT INTO %(tablename)s
(""",
  
  'mysql' : """
INSERT INTO %(tablename)s
(""",
}

  data_table_middle = {
  'postgresql' : """) 
VALUES
""",

  'mysql' : """)
VALUES
""",
}
  
  data_table_tail = {
  'postgresql' : ";\n",
  'mysql' : ";\n",
}
  sqlfilename = os.path.join(SQLDIR, "%s.insert.sql" % meta )
  sqlfile = io.BufferedWriter(io.FileIO(sqlfilename, "w"), 2048)
  
  ## types map has structure {'meta' : [(key, typename),..] , 'fit' : [(key, typename), ...]}
  try:
    # load pickled types_map
    with open(os.path.join(METADIR, "%s.map" % meta), "r") as f:
      types_map = cPickle.load(f)
  except:
    logging.info("You haven't run CREATE command yet. Doing it instead of you.")
    p_create(meta)
    try:
      with open(os.path.join(METADIR, "%s.map" % meta), "r") as f:
        types_map = cPickle.load(f)
    except:
      logging.error("I can not open the prepared default mapping file %s%s.map even it should exists" % (METADIR, meta))
      sys.exit()
  
  # write head of INSERT statement
  sqlfile.write(data_table_head[engine] % {"tablename" : tablename})
  delimiter = ""
  for k,v in types_map['meta']:
    sqlfile.write(delimiter)
    delimiter = ", "
    sqlfile.write(k)

  for k,v in types_map['fit']:
    sqlfile.write(delimiter)
    sqlfile.write(_get_key(k))
  
  sqlfile.write(data_table_middle[engine])
  
  # load time of youngest imported file
  last_indexed_file = os.path.join(METADIR + '%s_last_index' % meta)
  try:
    with open(last_indexed_file, "r") as f:
      last_indexed = utils.parse_datetime(f.read())
  except:
    last_indexed = datetime.datetime(1900, 1, 1)
  
  oldest = datetime.datetime(1900, 1, 1)
  
  # delimiter printed after each FOR (at begining '', in FOR ',', at the end ';'
  delimiter = ''
  
  # if some new FITS files were added
  changed = False
  
  ## now iterate all unindexed files
  for root,dirs,files in os.walk(data_source):
    for filename in files:
      filename = filename.strip()
      fitsfile = os.path.join(root, filename)[len(data_source):].lstrip(os.sep) # relative path from data_source WITHOUT os.sep at the begining
      
      if not filename.lower().endswith("fit") and not filename.lower().endswith("fits"):
        logging.debug("skipping file %s It does not ends with 'fit' or 'fits'" % fitsfile)
        continue
      
      ctime = datetime.datetime.fromtimestamp(os.path.getctime(os.path.join(data_source, fitsfile)))
      
      # skip already added
      if ctime <= last_indexed:
        logging.debug("skipping file %s It is younger than last imported file" % fitsfile)
        continue
      
      # save oldest file for updating .last_indexed file
      if ctime > oldest:
        oldest = ctime
      
      try:
        changed = True
        fimg = pyfits.open(os.path.join(data_source, fitsfile))
        cards = fimg[0].header.ascard
        
        logging.debug("Processing file: " + fitsfile)
        # list of TableCol which represents physical table
        table_cols = [
          utils.TableCol("META_CTIME", "BIGINT", ctime.strftime("%Y%m%d%H%M%S")),
          utils.TableCol("META_FILENAME", "varchar(50)", fitsfile),
          utils.TableCol("META_PERM", "smallint", 1),
        ]

        # apply all mappings
        table_cols.extend(_apply_mapping(cards, mandatory_map))
        if pgsphere:    
          table_cols.extend(_apply_mapping(cards, pgsphere_map))
        if local_map:
          table_cols.extend(_apply_mapping(cards, local_map))
        
        for item_k, item_t in types_map['fit']:
          try:
            key = _get_key(item_k)
            col = utils.TableCol(key)
            col.typename = item_t
            
            if isinstance(cards[item_k].value, pyfits.card.Undefined):
              col.value = None
            else:  
              col.parse_value(cards[item_k].value)
              
            if ("RA" == cards[item_k].key.upper() or "RA_" in cards[item_k].key.upper()) and col.value.__class__ == float:
              col.value *= 15.0

          except KeyError:
            logging.debug("No key %s. Setting NULL" % item_k)
            col.value = None
            
          table_cols.append(col)
        # end for item_k
        
        sqlfile.write(delimiter)
        
        sqlfile.write('(')
        subdelimiter=''
        for col in table_cols:
          sqlfile.write(subdelimiter)
          subdelimiter = ", "
          sqlfile.write(col.sql_insert())
        sqlfile.write(')')
        delimiter = ',\n'
        
        fimg.close()
      
      except:
        # if anything happens just throw the fits file out
        logging.info("File %s contains some FIT errors and could not be parsed" % fitsfile)
                
    # end for each filename
  
  # end for os.walk  
  sqlfile.write(data_table_tail[engine])    
  sqlfile.close()  
      
  with open(last_indexed_file, 'w') as f:
    f.write(str(oldest+datetime.timedelta(seconds=1)))
    
  print "The CTIME of the youngest imported file written in %s" % last_indexed_file
  print "SQL file written to %s" % sqlfilename
  
  
def h_list():
  print """\
  
Vypise seznam tabulek s FIT hlavickami a cestu k FIT souborum.\
      
  """
  
def p_list():
  db = connect()
  kurzor = db.cursor()
  data = sql_execute(kurzor, "SELECT * FROM admin", "num")

  cara = "+" + 22 * "-" + "+" + 52 * "-" + "+"
  print cara
  print "| %20s | %50s |" % ("name", "path")
  print cara
  for radek in data:
    print "| %20s | %50s |" % (radek[0], radek[1])
  print cara
        
  kurzor.close()
  db.commit()

def h_console():
  print """\

Spusti psql klienta, ktery umoznuje komunikovat z PostgreSQL
databazovym servrem.\
  
  """
  
def p_console():
  os.system("psql -W --host=%s %s %s" % (server, database, user))

def h_run():
  print """\

Prvni argument je nazev tabulky na, kterou se ma aplikovat jedna z
nasledujicich funkci, ktera je druhym argumentem:

  repair_date_obs   opravi date_obs v pripade, ze utmiddle presahlo pulnoc
  irepair_date_obs  pouze ukaze jake opravy by provedla predchozi funkce\
  
  """

def p_run():
  tabulka = sys.argv[2]
  funkce = sys.argv[3]
  
  if (funkce == "repair_date_obs"):
    prikaz = "SELECT * FROM repair_date_obs('%s', 'run');" % tabulka
    os.system("""psql -W -c "%s" --host=%s %s %s""" % (prikaz, server, database, user))
  elif (funkce == "irepair_date_obs"):
    prikaz = "SELECT * FROM repair_date_obs('%s', '');" % tabulka
    os.system("""psql -W -c "%s" --host=%s %s %s""" % (prikaz, server, database, user))
  
def h_changepath():
  print """\
  
Zmeni cestu k FIT souborum pro tabulku definovanou prvnim
argumentem.\
  
  """
  
def p_changepath():
  name = sys.argv[2]
  path = sys.argv[3]
  
  if (path[-1] != "/"):
    path = "%s/" % (path)

  db = connect()
  kurzor = db.cursor()
  sql_execute(kurzor, "UPDATE admin SET path = '%s' WHERE name like '%s'" % (path, name))
  kurzor.close()
  db.commit()
  print "changepath %s %s\nFinish..." % (name, path)
    
def h_tbdrop():
  print """\

Smaze tabulku definovanou prvnim argumentem.\
      
  """
  
def p_tbdrop():
  tb = sys.argv[2]
  odpoved = raw_input("Run tbdrop %s? (y/[n]) " % tb)
  
  if (odpoved.lower() in ["y", "yes"]):
    db = connect()
    kurzor = db.cursor()
    sql_execute(kurzor, "DELETE FROM admin WHERE name like '%s'" % (tb))
    sql_execute(kurzor, "DROP TABLE %s" % (tb))
    kurzor.close()
    db.commit()
    print "tbdrop %s\nFinish..." % tb
  else:
    print "tbdrop %s\nAbort..." % tb

def h_tbcopy():
  print """\
  
Zkopiruje tabulku definovanou prvnim argumentem do
tabulky definovanou druhym argumentem.\
  
  """
  
def p_tbcopy():
  tb1 = sys.argv[2]
  tb2 = sys.argv[3]
  
  db = connect()
  kurzor = db.cursor()
  sql_execute(kurzor, "CREATE TABLE %s AS SELECT * FROM %s" % (tb2, tb1))
  data = sql_execute(kurzor, "SELECT * FROM admin WHERE name like '%s'" % (tb1), "num")
  sql_execute(kurzor, "INSERT INTO admin VALUES ('%s', '%s')" % (tb2, data[0][1]))
  kurzor.close()
  db.commit()
  print "tbcopy %s %s\nFinish..." % (tb1, tb2)
    
def h_export():
  print """\

Ulozi zmeny z tabulky definovane prvnim argumentem zpet
do FIT souboru.\
      
  """
  
def p_export():
  table = sys.argv[2]
  
  db = connect()
  kurzor = db.cursor()
  data = sql_execute(kurzor, "SELECT * FROM admin WHERE name like '%s'" % (table), "num")
  path = data[0][1]
  
  key = sql_execute(kurzor, "SELECT * FROM %s " % table, "colnames")
  data = sql_execute(kurzor, "SELECT * FROM %s WHERE stav like 'Z'" % table, "num")
  
  for radek in data:
    try:
      print "Update file %s%s..." % (path, radek[1])
      fimg = pyfits.open("%s%s" % (path, radek[1]), mode = 'update')
    except IOError:
      print "Soubor '%s' se nepodarilo otevrit" % (radek[1])
      sys.exit(1)
  
    i = 2
    for radek_key in key:
      if ((radek_key != 'ID') and (radek_key != 'soubor') and (radek_key != 'stav')):
        klic = radek_key[4:].replace('__', '-').upper() 
        if ((radek[i] != '$') and (radek[i] != None)):
          try:
            fimg[0].header.update(klic, radek[i])
          except ValueError, chyba:
            print chyba
        else:
          del fimg[0].header[klic]
        i += 1
  
    sql_execute(kurzor, "UPDATE %s SET stav='U' WHERE ID=%s" % (table, str(radek[0])))

    fimg.flush()
    fimg.close()

  kurzor.close()
  db.commit()
  print "export %s\nFinish..." % table

def main():
  run = { "create": [p_create, 1, h_create],
          #~ "tbdrop": [p_tbdrop, 1, h_tbdrop],
          #~ "tbcopy": [p_tbcopy, 2, h_tbcopy],
          "import": [p_import, 1, h_import],
          "reset": [p_reset, 1, h_reset],
          #~ "install": [p_install, 0, h_install],
          #~ "uninstall": [p_uninstall, 0, h_uninstall],
          #~ "list": [p_list, 0, h_list],
          #~ "console": [p_console, 1, h_console],
          #~ "run": [p_run, 2, h_run],
          #~ "changepath": [p_changepath, 2, h_changepath],
          #~ "export": [p_export, 1, h_export] 
          } 

  pocet = len(sys.argv)
  if (pocet == 1):
    help()
    sys.exit(0)
    
  pocet -= 2
  prikaz = sys.argv[1]
      
  if ((run.has_key(prikaz)) and (pocet == 1) and (sys.argv[2] in ["help", "h", "?"])):
    run[prikaz][2]()
  elif ((run.has_key(prikaz)) and (run[prikaz][1] == pocet)):
    run[prikaz][0]()
  else:
    help()
  
if __name__ == '__main__':
  main()
