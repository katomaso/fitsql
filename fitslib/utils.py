# coding:utf-8
import datetime
import sys
import re
import exceptions
import logging
from pyfits.card import Undefined
from ConfigParser import ConfigParser, NoOptionError

CONFFILE = "fitsql.conf"

def get_conf(meta, mandatory=None):
  '''Returns dictionary of options from [global] and updated from [meta] meta'''
  conf = {}
  parser = ConfigParser()
  parser.read(CONFFILE)
  
  if not parser.has_section(meta):
    logging.error("No config file %s with meta %s" % CONFFILE, meta)
    raise KeyError('No section %s' % meta)
  
  for option in parser.items("globals"):
    conf[option[0]] = option[1]
  
  for option in parser.items(meta):
    conf[option[0]] = option[1]
  
  
  if conf.get('debug', 'off').lower() in ['on', '1', 'true']:
    conf['debug'] = True
  else:
    conf['debug'] = False
  
  if conf.get('pgsphere', 'off').lower() in ['on', '1', 'true']:
    conf['pgsphere'] = True
  else:
    conf['pgsphere'] = False
      
  if mandatory:
    for m in mandatory:
      if not m in conf:
        raise KeyError("In config file missing %s" % m)
        
  return conf
  
  
class SuccessError(exceptions.Exception):
  pass
  
  
class TableCol(object):
  
  comment = ''  
  default = ''
  
  def __init__(self):
    self.key = ""
    self.typename = ""
    self.value = ""
  
  def __init__(self, key, typename="",value="", default="",comment=""):
    self.key = key
    self.typename = typename
    self.default = default
    self.comment = comment
    self.value = value
      
  def sql_create(self):
    ''' Returns string for CREATE TABLE statement'''
    out = "%s %s" % (self.key.ljust(18), self.typename.ljust(12))
    if self.default != "":
      out += "\t%s" % self.default.upper()
    else:
      out += "\tNULL"
      
    if self.comment != "":
      out += ",\t# %s\n" % self.comment
    else:
      out += ",\n"
      
    return out
  
  def sql_insert(self):
    if self.value is None:
      return "NULL"
    if isinstance(self.value, (int,float,)):
      return str(self.value)
    
    if "char" in self.typename.lower():  
      return "\"%s\"" % self.value
      
    if "date" in self.typename.lower() or "time" in self.typename.lower():  
      return "\"%s\"" % self.value
      
    return str(self.value)
    
  def parse_value(self, value):
    '''Parses right type prom value given self.typename'''
    if value is None or isinstance(value, Undefined):
      self.value = None
      
    if self.typename in ('date', 'time', 'datetime'):
      self.value = parse_datetime(value.strip())
      return
    
    if self.typename in ('float', 'double'):
      if value.__class__ == float:
        self.value = value
        return
      self.value = parse_DMS(value)
      if self.value is None:
        try:
          self.value = float(value)
        except:
          self.value = None
      return
    
    if self.typename == 'int':
      self.value = int(value)
      return
    
    # treat it as a string
    self.value = value
      
  def __str__(self):
    return "%s\t%s\t%s" % (self.key, self.typename, str(self.value))
    
class DBValue(object):
  default = ''
  typename = ''
  
  def __init__(self):
    self.key = ""
    self.value = ""
    self.comment = ""

  def __init__(self, key, name, value):
    self.key = key
    self.name = name
    self.value = value
  
  def sql_insert(self):
    ''' Returns string for INSERT INTO statement'''
    out = "('%s', '%s', '%s')" % (self.key, self.name, self.value)  
    return out
  
  def __str__(self):
    return "('%s', '%s', '%s')" % (self.key, self.name, self.value)

class Connector(object):
  db = None
  
  def __init__(self, meta):
    try:
      conf = get_conf(meta, ['server', 'database', 'user', 'password', 'engine'])
      
      if conf['engine'] == "postgresql":
        import pg
        self.db = pg.connect(host=conf['server'], database=conf['database'], user=conf['user'], password=conf['password'])
        self.execute = self._pg_execute
      
      if conf['engine'] == "mysql":
        import MySQLdb as mysql
        self.db = mysql.connect(host=conf['server'], user=conf['user'], passwd=conf['password'], db=conf['database'])
        self.execute = self._mysql_execute
        
    except Exception as e:
      logging.error("Connection to database '%s' failed" % conf['database'])
      logging.error(e)
      sys.exit(1)

  
  def execute(self, command):
    raise NotImplemented("Must be overriden")
  
    
  def _mysql_execute(self, command):
    self.db.query(command)
  
  
  def _pg_execute(self, command):
    self.db.cursor.execute(command)
    self.db.cursor.commit()
    
    
  def close(self):
    if self.db is not None:
      self.db.close()
      self.db = None
    
    
        
class SPoint(DBValue):
  '''
  SPoint structure in pgsphere
  '''
  typename = "SPoint"
  
  def __init__(self, key):
    self.key = key
    self.ra = 0.0
    self.dec = 0.0
  
  def __init__(self, key, ra, dec):
    self.key = key
    if isinstance(ra, str):
      self.ra = self.parse_float(ra)
    else:
      self.ra = ra
      
    if isinstance(dec, str):
      self.dec = self.parse_float(dec)
    else:
      self.dec = dec
  
  def parse_float(self, value):
    r = parse_DMS(value)
    if r is not None:
      return r
    return float(value)
    
  def __str__():
    return "SPoint(%0.6f,%0.6f)" % self.ra, self.dec
    
    
def parse_datetime(value):
  '''
  Returns date or time or datetime
  throws ValueError
  '''
  time_formats = [
    r"^(?P<hour>\d{1,2}):(?P<minute>\d{1,2}):(?P<sec>\d{1,2})$",
  ]
  date_formats = [
    r"(?P<day>\d{1,2})\.(?P<month>\d{1,2})\.(?P<year>\d{2,4})",
    r"(?P<day>\d{1,2})/(?P<month>\d{1,2})/(?P<year>\d{2,4})",
  ]
  datetime_formats = [
    r"(?P<year>\d{2,4})\-(?P<month>\d{1,2})\-(?P<day>\d{1,2})[T ](?P<hour>\d{1,2}):(?P<minute>\d{1,2}):(?P<sec>\d{1,3})", #'2009-07-17T02:57:46'
    r"(?P<hour>\d{1,2}):(?P<minute>\d{1,2}):(?P<sec>\d{1,2}) \((?P<day>\d{1,2})/(?P<month>\d{1,2})/(?P<year>\d{2,4})\)", # 19:38:32 (26/10/2006)
  ]
  
  for datetime_format in datetime_formats:
    res = re.match(datetime_format, value)
    if res is not None:
      return datetime.datetime(int(res.group("year")), int(res.group("month")), int(res.group("day")),
                               int(res.group("hour")),int(res.group("minute")),int(res.group("sec")))
      
  for date_format in date_formats:
    res = re.match(date_format, value)
    if res is not None:
      year = int(res.group("year"))
      if year < 100:
        year += 1900
      return datetime.date(year, int(res.group("month")), int(res.group("day")))
      
  for time_format in time_formats:
    res = re.match(time_format, value)
    if res is not None:
      return datetime.time(int(res.group("hour")), int(res.group("minute")), int(res.group("sec")))
      
  return None

def parse_DMS(value):
  '''
  DMS is float number given in format DD:MM:SS.S (degrees, minutes, seconds)
  return float
  '''
  if value is None:
    return None

  if isinstance(value, (float, int,)):
    return float(value)
    
  try:  
    pattern = r"([\+-])?(?P<degree>\d{1,2}):(?P<minute>\d{1,2}):(?P<sec>\d{1,2}\.\d{1,3})"
    res = re.match(pattern, value)
    value = value.strip()
    if res is not None:
      angle = float(res.group('degree'))
      angle += float(res.group('minute'))/60.0
      angle += float(res.group('sec'))/3600
      try:
        if res.group("sign") == '-': return -1*angle
      except:
        return angle
      
    return None
  except:
    logging.error(str(type(value)) + ", " + str(value))
    raise
  
if __name__ == "__main__":
  '''
  lets run some tests
  '''
  ret = parse_DMS("18:37:05.3")
  print 'parse_DMS("18:37:05.3") returns %0.6f should be 18.618139 =>' % ret,
  if round(ret, 6) != 18.618139:
    print "failed"
  else:
    print "succeded"
  
  ret = parse_DMS("38:48:47.2")
  print 'parse_DMS("38:48:47.2") returns %0.6f should be 38.813111 =>' % ret,
  if round(ret,6) != 38.813111:
    print "failed"
  else:
    print "succeded"
