''' @file example.py
Dictionary mapping maps FIT keywords into new columns in database
'''

'''
Tuple of dictionaries. Must be named mipping!
@key keys - FIT keyword from given FIT file. The will be mappend in given order to arguments of lambda function
@key map - lambda function which has to return dict {key,typename,value}
			- key: name of column in database ( - replace for __, use uppercase)
			- typename: type of value in database. Use only valid types for current database
			- value: computed value for each fit file
			[ - default: the third part in CREATE statement for each column (default "NULL")]
'''

mapping = (
{
 'keys' : ("CRVAL1", "NAXIS1" ,"CDELT1"),
 'map'  : lambda a,b,c: { 	'key' : "META_SPEC_MIDDLE", 
							'typename' : "datetime",
							'value' : ((b-1)/2.0*c)+a, # means ((NAXIS1 - 1) / 2.0 * CDELT1) + CRVAl1
							'default' : "NOT NULL"
							}
 },
 {
 'keys' : ( "NAXIS1", "CDELT1"),
 'map' : lambda b, c: { "key" : "META_SPEC_EXT", 
						"typename" : "float",
						"value" : float((b-1)*c), # means (NAXIS1 - 1) * CDELT1
						}
 },
)
